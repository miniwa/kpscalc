import 'package:flutter/material.dart';
import 'package:kpscalc/app.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final ThemeData mainTheme = ThemeData(
    brightness: Brightness.dark,
    primarySwatch: Colors.grey,
    scaffoldBackgroundColor: Colors.black, //backgroubd color
    accentColor: Colors.pinkAccent,
    canvasColor: Colors.black, //bottomnavbar bgcolor
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'kpscalc',
      theme: mainTheme,
      home: Application(),
    );
  }
}
