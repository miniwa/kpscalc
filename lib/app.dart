import 'package:flutter/material.dart';
import 'package:kpscalc/pages/calc.dart';
import 'package:kpscalc/pages/speed.dart';

// Application or Landing Page
class Application extends StatefulWidget {
  Application({Key key}) : super(key: key);

  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  // For the BottomNavigationBar
  int _selectedPageIndex = 0; //The app starts up with the homePage (index 1) as the open widget
  static List<Widget> _pageOptions = <Widget>[
    Calculator(),
    Speed(),
  ];

  void _onPageItemTapped(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  BottomNavigationBarItem bottomNavBarItem(
      BuildContext context, IconData icon, String title) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      title: Text(
        title,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, fontFamily: "Roboto Thin"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSwitcher(
        child: _pageOptions.elementAt(_selectedPageIndex),
        duration: const Duration(milliseconds: 200),
        transitionBuilder: (Widget child, Animation<double> animation) {
            return FadeTransition(child: child, opacity: animation);
        },
      ),
      // The BottomNavigationBar
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          bottomNavBarItem(context, Icons.keyboard, 'Calculator', ),
          bottomNavBarItem(context, Icons.timer, 'Speed'),
        ],
        currentIndex: _selectedPageIndex,
        onTap: _onPageItemTapped,
      ),
    );
  }
}