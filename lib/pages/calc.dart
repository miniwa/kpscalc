import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  Calculator({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  double _bpm = 0;
  double _notetype = 0;
  double _musicrate = 1;
  double _calced = 0;

  _setbpm(String lol) {
    setState(() {
      _bpm = double.parse(lol);
    });
    _ababa();
  }
  _setnotes(String lol) {
    setState(() {
      _notetype = double.parse(lol);
    });
    _ababa();
  }
  _setrate(String lol) {
    setState(() {
      _musicrate = double.parse(lol);
    });
    _ababa();
  }

  _ababa() {
    if (_bpm != 0 && _notetype != 0 && _musicrate != 0) {
      setState(() {
        _calced = (_bpm * _musicrate / 60) / (4 / _notetype);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(20.0),
            child: Column(
            children: <Widget>[
              Text(
                'kps calculator',
                style: TextStyle(color: Colors.white, fontSize: 45, fontFamily: "Roboto Thin"),
                textAlign: TextAlign.center,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(60, 0, 60, 0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      style: TextStyle(fontFamily: "Roboto Thin"),
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: 'bpm'),
                      keyboardType: TextInputType.number,
                      onChanged: _setbpm,
                    ),
                    TextField(
                      style: TextStyle(fontFamily: "Roboto Thin"),
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: 'note type'),
                      keyboardType: TextInputType.number,
                      onChanged: _setnotes,
                    ),
                    TextField(
                      style: TextStyle(fontFamily: "Roboto Thin"),
                      decoration: InputDecoration(
                          border: InputBorder.none, hintText: 'music rate'),
                      keyboardType: TextInputType.number,
                      onChanged: _setrate,
                    ),
                  ],
                ),
              ),
              Text(
                '$_calced',
                style: TextStyle(color: Colors.white, fontSize: 30, fontFamily: "Roboto Thin"),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
