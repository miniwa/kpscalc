import 'package:flutter/material.dart';

class Speed extends StatefulWidget {
  Speed({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SpeedState createState() => _SpeedState();
}

class _SpeedState extends State<Speed> {
  bool _start = false;
  int _pressed = 0;
  int _kpsInt = 0;
  int _lastMs = 0;
  int _bpm = 0;
  String _kpsFloat = "0";
  var _plumacute = new Stopwatch();

  _press() {
    if (_start == false) {
      _startTimer();
      setState(() {
        _start = true;
        _kpsInt = 1;
        _bpm = 0;
        _kpsFloat = "0";
      });
    } else {
      if (_lastMs - _plumacute.elapsedMilliseconds < -2000) {
        _plumacute.reset();
        setState(() {
          _pressed = 0;
          _kpsInt = 1;
          _kpsFloat = "0";
          _lastMs = 0; 
          _bpm = 0;
        });
      } else {
        setState(() {
          _pressed++;
          _kpsInt = (_pressed / (_plumacute.elapsedMilliseconds/1000)).floor();
          _kpsFloat = (_pressed / (_plumacute.elapsedMilliseconds/1000)).toStringAsFixed(3).split(".")[1];
          _lastMs = _plumacute.elapsedMilliseconds;
          _bpm = ((_pressed / (_plumacute.elapsedMilliseconds/1000))*60).round();
        });
      }
    }
  }

  _startTimer() {
    _plumacute.start();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.black,
                child: Center(
                  child: Text('$_kpsInt\n$_kpsFloat\n\n$_bpm', textAlign: TextAlign.center, style: TextStyle(fontSize: 30, fontFamily: "Roboto Thin"),), //THIS IS REALLY DUMB
                ),
              ),
            )
          ],
        ),
        onTap: () {
          _press();
        },
      ),
    );
  }
}
